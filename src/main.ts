import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // init config service
  const configService: ConfigService = app.get(ConfigService);
  await app.listen(3000);
}
bootstrap();
