import {
  Body,
  Controller,
  Post,
  Get,
  Res,
  Req,
  Param,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { JwtAuthGuard } from 'src/auth/strategy/jwt-auth.guard';
import { ItemsDto } from './dtos/items.dto';
import { ItemsService } from './items.service';

@Controller('items')
export class ItemsController {
  constructor(private itemService: ItemsService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async createItem(@Body() body, @Res() res) {
    let isOk = false;
    const item = new ItemsDto();
    console.log(body);
    item.description = body.description;
    item.name = body.name;
    item.quantity = body.quantity;
    item.url = body.url;

    await validate(item).then((errors) => {
      if (errors.length > 0) {
        console.log(errors);
      } else {
        isOk = true;
      }
    });

    if (isOk) {
      const result = await this.itemService.addItem(item);
      res.status(result.code).json(result.content);
    } else {
      res.status(400).json({ msg: 'Invalid request' });
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async listItems(@Res() res) {
    const result = await this.itemService.getItems();
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Res() res, @Param() params) {
    const result = await this.itemService.getItem(params.id);
    res.status(result.code).json(result.content);
  }
}
