import { IsNumber, IsString } from 'class-validator';

export class ItemsDto {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsNumber()
  quantity: number;

  @IsString()
  url: string;
}
